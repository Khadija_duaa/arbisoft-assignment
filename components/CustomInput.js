import React from 'react';
import {View, TouchableOpacity, Text, TextInput, StyleSheet} from 'react-native';
import colors from '../utils/colors';

const styles = StyleSheet.create({
    container: {
        backgroundColor: colors.white,
        flexDirection: 'row',
        marginVertical: 10,
        marginHorizontal: 20,
        borderRadius: 6,
    },
    button: {
        padding: 15,
        borderRightColor: colors.border,
        borderRightWidth: 1,
        backgroundColor: colors.white,
        borderTopLeftRadius: 5,
        borderBottomLeftRadius: 5,
    },
    buttonText: {
        fontSize: 19,
        color: colors.blue,
        fontWeight: 'bold',
    },
});

export const CustomInput = (props) => {


    return (
        <View style={styles.container} {...props}>
            <TouchableOpacity style={styles.button} onPress={props.onButtonPress} {...props}>
                <Text style={styles.buttonText} {...props}>{props.text}</Text>
            </TouchableOpacity>
            <TextInput style={{
                flex: 1,
                padding: 10,
                fontSize: 16,
                color: colors.textLight,
                backgroundColor:!props.editable ? 'silver' : colors.white}} {...props} />
        </View>
    );
};