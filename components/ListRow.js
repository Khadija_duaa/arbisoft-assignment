import React from 'react';
import { StyleSheet, TouchableOpacity, Text, View } from 'react-native';
import colors from '../utils/colors';

const styles = StyleSheet.create({
    row: {
        paddingVertical: 15,
        paddingHorizontal: 20,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: colors.white,
    },
    title: {
        color: colors.text,
        fontSize: 17,
    },
    titleBold: {
        color: colors.text,
        fontWeight: 'bold',
        fontSize: 17,
    },
});

export const ListRow = (props) => (
    <TouchableOpacity onPress={props.onPress} style={styles.row} {...props}>
        <Text style={ props.bold ? styles.titleBold : styles.title} {...props}>{props.title}</Text>
        {props.rightIcon}
    </TouchableOpacity>
);
