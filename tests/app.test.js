import React from 'react';
import App from '../App';
import {_Home} from '../containers/Home';
import {_CurrencyList} from '../containers/CurrencyList';
import {Options} from '../containers/Options';
import {_SignIn} from '../containers/SignIn';
import {_Themes} from '../containers/Themes';
import {ListRow} from '../components/ListRow';
import {CustomInput} from '../components/CustomInput';
import {ReverseButton} from '../components/ReverseButton';
import {Separator} from '../components/Separator';
import renderer from 'react-test-renderer';
import reducer, {
    setCurrencyRates,
    setProcessing,
    setThemeColor
} from '../stores/currency-conversion/currency-conversion-reducer';
import {initState} from '../stores/currency-conversion/currency-conversion-reducer';
import store from "../stores";
import {SafeAreaProvider} from "react-native-safe-area-context";
import {CustomButton} from "../components/CustomButton";

describe('todos app', () => {

    it('renders correctly', () => {
        const tree = renderer.create(<App/>).toJSON();
        expect(tree).toMatchSnapshot();
    });

    it('App.js has 0 child', () => {
        const tree = renderer.create( <SafeAreaProvider store={store}><App /></SafeAreaProvider>).toJSON();
        expect(tree.children && tree.children.length).toBe(null);
    });
});

describe('todos CustomButton', () => {

    it('renders correctly', () => {
        const tree = renderer.create(<CustomButton/>).toJSON();
        expect(tree).toMatchSnapshot();
    });

    it('CustomButton.js has 0 child', () => {
        const tree = renderer.create( <SafeAreaProvider store={store}><CustomButton/></SafeAreaProvider>).toJSON();
        expect(tree.children && tree.children.length).toBe(null);
    });
});

describe('todos ListRow', () => {

    it('renders correctly', () => {
        const tree = renderer.create(<ListRow/>).toJSON();
        expect(tree).toMatchSnapshot();
    });

    it('ListRow.js has 0 child', () => {
        const tree = renderer.create( <SafeAreaProvider store={store}><ListRow/></SafeAreaProvider>).toJSON();
        expect(tree.children && tree.children.length).toBe(null);
    });
});

describe('todos CustomInput', () => {

    it('renders correctly', () => {
        const tree = renderer.create(<CustomInput/>).toJSON();
        expect(tree).toMatchSnapshot();
    });

    it('CustomInput.js has 0 child', () => {
        const tree = renderer.create( <SafeAreaProvider store={store}><CustomInput/></SafeAreaProvider>).toJSON();
        expect(tree.children && tree.children.length).toBe(null);
    });
});

describe('todos ReverseButton', () => {

    it('renders correctly', () => {
        const tree = renderer.create(<ReverseButton/>).toJSON();
        expect(tree).toMatchSnapshot();
    });

    it('ReverseButton.js has 0 child', () => {
        const tree = renderer.create( <SafeAreaProvider store={store}><ReverseButton/></SafeAreaProvider>).toJSON();
        expect(tree.children && tree.children.length).toBe(null);
    });
});

describe('todos Separator', () => {

    it('renders correctly', () => {
        const tree = renderer.create(<Separator/>).toJSON();
        expect(tree).toMatchSnapshot();
    });

    it('Separator.js has 0 child', () => {
        const tree = renderer.create( <SafeAreaProvider store={store}><Separator/></SafeAreaProvider>).toJSON();
        expect(tree.children && tree.children.length).toBe(null);
    });
});



describe('todos Home', () => {

    it('renders correctly', () => {
        const tree = renderer.create(<SafeAreaProvider store={store}><_Home /></SafeAreaProvider>).toJSON();
        expect(tree).toMatchSnapshot();
    });


    it('Home.js has 0 child', () => {
        const tree = renderer.create( <SafeAreaProvider store={store}><_Home /></SafeAreaProvider>).toJSON();
        expect(tree.children && tree.children.length).toBe(null);
    });

    let findElement =  function( tree, element)
    {
        console.warn(tree);
        return true;
    };

    const  tree= renderer.create(  <SafeAreaProvider store={store}><_Home /></SafeAreaProvider>).toJSON();

    it('defined setBaseCurrency', () => {
        expect(findElement(tree, 'setBaseCurrency')).toBeDefined();
    });

    it('defined setConversionRate', () => {
        expect(findElement(tree, 'setConversionRate')).toBeDefined();
    });

    it('defined swapCurrencies', () => {
        expect(findElement(tree, 'swapCurrencies')).toBeDefined();
    });

    it('defined setQuoteCurrency', () => {
        expect(findElement(tree, 'setQuoteCurrency')).toBeDefined();
    });

});

describe('todos CurrencyList', () => {

    it('renders correctly', () => {
        const tree = renderer.create(<SafeAreaProvider store={store}><_CurrencyList/></SafeAreaProvider>).toJSON();
        expect(tree).toMatchSnapshot();
    });

    it('CurrencyList.js has 0 child', () => {
        const tree = renderer.create( <SafeAreaProvider store={store}><_CurrencyList/></SafeAreaProvider>).toJSON();
        expect(tree.children && tree.children.length).toBe(null);
    });

    let findElement =  function( tree, element)
    {
        console.warn(tree);
        return true;
    };

    const  tree= renderer.create(  <SafeAreaProvider store={store}><_CurrencyList/></SafeAreaProvider>).toJSON();

    it('defined onIconPress', () => {
        expect(findElement(tree, 'onIconPress')).toBeDefined();
    });
});


describe('todos Options', () => {

    it('renders correctly', () => {
        const tree = renderer.create(<SafeAreaProvider store={store}><Options/></SafeAreaProvider>).toJSON();
        expect(tree).toMatchSnapshot();
    });


    let findElement =  function( tree, element)
    {
        console.warn(tree);
        return true;
    };

    const  tree= renderer.create(  <SafeAreaProvider store={store}><Options/></SafeAreaProvider>).toJSON();

    it('defined onIconPress', () => {
        expect(findElement(tree, 'openExternalLink')).toBeDefined();
    });
});


describe('todos SignIn', () => {

    it('renders correctly', () => {
        const tree = renderer.create(<SafeAreaProvider store={store}><_SignIn/></SafeAreaProvider>).toJSON();
        expect(tree).toMatchSnapshot();
    });

    it('SignIn.js has 0 child', () => {
        const tree = renderer.create( <SafeAreaProvider store={store}><_SignIn/></SafeAreaProvider>).toJSON();
        expect(tree.children && tree.children.length).toBe(null);
    });

    let findElement =  function( tree, element)
    {
        console.warn(tree);
        return true;
    };

    const  tree= renderer.create(  <SafeAreaProvider store={store}><_SignIn/></SafeAreaProvider>).toJSON();

    it('defined utilFunc', () => {
        expect(findElement(tree, 'utilFunc')).toBeDefined();
    });

    it('defined onButtonPress', () => {
        expect(findElement(tree, 'onButtonPress')).toBeDefined();
    });
});

describe('todos Themes', () => {

    it('renders correctly', () => {
        const tree = renderer.create(<SafeAreaProvider store={store}><_Themes/></SafeAreaProvider>).toJSON();
        expect(tree).toMatchSnapshot();
    });

    it('Themes.js has 0 child', () => {
        const tree = renderer.create( <SafeAreaProvider store={store}><_Themes/></SafeAreaProvider>).toJSON();
        expect(tree.children && tree.children.length).toBe(null);
    });
});

describe('todos reducer', () => {

    it('should return the initial state baseCurrency', () => {
        expect(reducer(undefined, {})).toMatchSnapshot()
    });

    it('should handle CURRENCY_RATES TODO_REQUEST', () => {
        expect(
            reducer(initState,
                {
                    type: 'CURRENCY_RATES'
                })
        ).toMatchSnapshot()
    });

    it('should handle THEME_COLOR TODO_REQUEST', () => {
        expect(
            reducer(initState,
                {
                    type: 'THEME_COLOR'
                })
        ).toMatchSnapshot()
    });

    it('should handle SET_USER_AUTHENTICATED TODO_REQUEST', () => {
        expect(
            reducer(initState,
                {
                    type: 'SET_USER_AUTHENTICATED'
                })
        ).toMatchSnapshot()
    });

    it('should handle PROCESSING TODO_REQUEST', () => {
        expect(
            reducer(initState,
                {
                    type: 'PROCESSING'
                })
        ).toMatchSnapshot()
    });

    it('should return the initial state color', () => {
        setThemeColor(initState, 'red');
        expect(initState.color).toBe('red')
    });

    it('should return the initial state currencyRateList', () => {
        setCurrencyRates(initState, []);
        expect(initState.currencyRateList).toStrictEqual([])
    });

    it('should return the initial state processing', () => {
        setProcessing(initState, false);
        expect(initState.processing).toBe(false)
    });

});



