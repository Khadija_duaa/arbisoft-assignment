import React, {useEffect} from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import {Provider} from 'react-redux';
import {_Home} from "./containers/Home";
import {Options} from "./containers/Options"
import {_Themes} from './containers/Themes';
import {BackHandler} from "react-native";
import {_CurrencyList} from "./containers/CurrencyList";
import store from "./stores";
import {_SignIn} from "./containers/SignIn";

const MainStack = createStackNavigator();

const MainStackScreen = () => (
    <MainStack.Navigator initialRouteName="SignIn">
        <MainStack.Screen name="SignIn" component={_SignIn} options={{ headerShown: false }}/>
        <MainStack.Screen name="Home" component={_Home} options={{ headerShown: false }}/>
        <MainStack.Screen name="Options" component={Options} />
        <MainStack.Screen
            name="CurrencyList"
            component={_CurrencyList}
            options={({ route }) => ({
                title: route.params.title,
                activeCurrency: route.params.activeCurrency,
            })} />
        <MainStack.Screen name="Themes" component={_Themes} />
    </MainStack.Navigator>
);

export default () => {
        const backAction = () => {
            return false;
        };

        useEffect(() => {
            BackHandler.addEventListener("hardwareBackPress", backAction);

            return () =>
                BackHandler.removeEventListener("hardwareBackPress", backAction);
        }, []);
    return (
    <Provider store={store}>
        <NavigationContainer>
            <MainStackScreen/>
        </NavigationContainer>
    </Provider>
    )
};





