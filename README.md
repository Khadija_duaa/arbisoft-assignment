# Setup and Installations

First and foremost things that you should have installed on your terminal to run this project locally.

If you are using Windows


**Environment :**

1. OS : Windows 10 Pro - Version 1903
2. Node
3. npm
4. Java SE (jdk)
5. Android Studio
6. WebStorm : 2018.1.4 - Build Version WS-181.5087.27 (or any other IDE, you are
comfortable with)

**Packages :**

1. react
2. react-native


**After these installations you need to clone this project**

1. Run this command in a directory where you want to clone it : git clone <project-url>

2. Go inside the cloned project directory and run : npm install (this command reads the
package.json file from react-native project directory and locally install all those
dependencies at your terminal)

3. Run 'npm start' to run this project locally


**Code Coverage Stats after testing through Jest :**

![picture](assets/images/coverage.png)