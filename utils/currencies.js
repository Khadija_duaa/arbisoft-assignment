export default [
    {
        title: 'AUD',
        iconName: 'heart-outlined',
    },
    {
        title: 'BGN',
        iconName: 'heart-outlined',
    },
    {
        title: 'BRL',
        iconName: 'heart-outlined',
    },
    {
        title: 'CAD',
        iconName: 'heart-outlined',
    },
    {
        title: 'CHF',
        iconName: 'heart-outlined',
    },
    {
        title: 'CNY',
        iconName: 'heart-outlined',
    },
    {
        title: 'CZK',
        iconName: 'heart-outlined',
    },
    {
        title: 'DKK',
        iconName: 'heart-outlined',
    },
    {
        title: 'EUR',
        iconName: 'heart-outlined',
    },
    {
        title: 'GBP',
        iconName: 'heart-outlined',
    },
    {
        title: 'HKD',
        iconName: 'heart-outlined',
    },
    {
        title: 'HRK',
        iconName: 'heart-outlined',
    },
    {
        title: 'HUF',
        iconName: 'heart-outlined',
    },
    {
        title: 'IDR',
        iconName: 'heart-outlined',
    },
    {
        title: 'ILS',
        iconName: 'heart-outlined',
    },
    {
        title: 'INR',
        iconName: 'heart-outlined',
    },
    {
        title: 'JPY',
        iconName: 'heart-outlined',
    },
    {
        title: 'KRW',
        iconName: 'heart-outlined',
    },
    {
        title: 'MXN',
        iconName: 'heart-outlined',
    },
    {
        title: 'MYR',
        iconName: 'heart-outlined',
    },
    {
        title: 'NOK',
        iconName: 'heart-outlined',
    },
    {
        title: 'NZD',
        iconName: 'heart-outlined',
    },
    {
        title: 'PHP',
        iconName: 'heart-outlined',
    },
    {
        title: 'PLN',
        iconName: 'heart-outlined',
    },
    {
        title: 'RON',
        iconName: 'heart-outlined',
    },
    {
        title: 'RUB',
        iconName: 'heart-outlined',
    },
    {
        title: 'SEK',
        iconName: 'heart-outlined',
    },
    {
        title: 'SGD',
        iconName: 'heart-outlined',
    },
    {
        title: 'THB',
        iconName: 'heart-outlined',
    },
    {
        title: 'TRY',
        iconName: 'heart-outlined',
    },
    {
        title: 'USD',
        iconName: 'heart-outlined',
    },
    {
        title: 'ZAR',
        iconName: 'heart-outlined',
    },

]
