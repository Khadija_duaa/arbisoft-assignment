export default {
    white: '#fff',
    border: '#E2E2E2',
    text: '#808080',
    textLight: '#797979',
};