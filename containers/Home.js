import React from 'react';
import { View,
    StyleSheet,
    StatusBar,
    Dimensions,
    Image,
    Animated,
    Easing,
    Alert,
    Text,
    ScrollView,
    Keyboard,
    TouchableOpacity,
    KeyboardAvoidingView,
    ActivityIndicator,
    BackHandler
} from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import moment from 'moment';
import { Entypo } from '@expo/vector-icons';
import {connect} from 'react-redux';
import { CustomInput } from '../components/CustomInput';
import {ReverseButton} from "../components/ReverseButton";
import {loadCurrencyRates} from "../stores/currency-conversion/currency-conversion-actions";
import colors from '../utils/colors';

const screen = Dimensions.get('window');

class Home extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            spinAnim: new Animated.Value(0),
            IsScrollEnabled: false,
            baseCurrency:'USD',
            quoteCurrency:'GBP',
            baseCurrencyUserValue:'1' ,
            conversionRate: 0.89824,
            IsLogoRotationEnabled: false
        };
        Animated.loop(Animated.timing(
            this.state.spinAnim,
            {
                toValue: 1,
                duration: 3000,
                useNativeDriver: true
            }
        )).start();

    }


    componentDidMount() {
        this.setState({baseCurrency: this.props.baseCurrency, quoteCurrency: this.props.quoteCurrency,
            baseCurrencyUserValue: this.props.baseCurrencyUserValue, conversionRate: this.props.conversionRate});
        this.props.loadCurrencyRates(this.state.baseCurrency);
    }

    UNSAFE_componentWillMount () {

        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this.keyboardDidShow);
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this.keyboardDidHide);
    }

    keyboardDidShow =() => {
        this.setState({IsScrollEnabled: true, IsLogoRotationEnabled : true});
    };

    keyboardDidHide = () => {
        this.setState({IsScrollEnabled: false, IsLogoRotationEnabled : false});
    };

    componentWillUnmount () {
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
    }



    styles = StyleSheet.create({

        logoContainer: {
            alignItems: 'center',
            justifyContent: 'center',
            marginBottom: 9,
        },

        logoBackground: {
            width: screen.width * 0.45,
            height: screen.width * 0.45,
        },

        logo: {
            position: 'absolute',
            width: screen.width * 0.25,
            height: screen.width * 0.25,
        },
        appName: {
            fontWeight: 'bold',
            fontSize: 27,
            color: colors.white,
            textAlign: 'center',
        },
        text: {
            fontSize: 13,
            color: colors.white,
            textAlign: 'center',
        },
        settingIcon: {
            alignItems: 'flex-end',
            marginHorizontal: 18,
        },
    });

    setConversionRate = (currency, currencyRateList) =>{

        for( let i=0 ; i< currencyRateList.length ; i++)
        {
            if( currency === currencyRateList[i].title)
            {
                this.setState({conversionRate : currencyRateList[i].rate},
                    ()=>{ console.log('its conv', this.state.conversionRate)});
                break;
            }
        }
    };

    swapCurrencies = (currencyRateList) =>{

        this.props.loadCurrencyRates(this.state.quoteCurrency);
        this.setConversionRate(this.state.quoteCurrency,currencyRateList);
        this.setState({
            baseCurrency: this.state.quoteCurrency,
            quoteCurrency: this.state.baseCurrency,
        })
    };

    setBaseCurrency = (currency, currencyRateList) =>{

        if(currency === this.state.quoteCurrency)
        {
            Alert.alert('Base and Quote currencies should be different')
        }
        else {
            this.props.loadCurrencyRates(currency);
            this.setConversionRate(currency, currencyRateList);
            this.setState({
                baseCurrency: currency
            })
        }
    };



    setQuoteCurrency = (currency,currencyRateList) =>{

        if(currency === this.state.baseCurrency)
        {
            Alert.alert('Base and Quote currencies should be different')
        }
        else
        {
            this.props.loadCurrencyRates(currency);
            this.setConversionRate(currency,currencyRateList);
            this.setState({
                quoteCurrency: currency
            })
        }


    };


    render() {
        const today = new Date();
        const currentDate = moment(today).format("MMMM D, YYYY");
        const spinShow = this.state.spinAnim.interpolate({
            inputRange: [0, 1],
            outputRange: ['0deg', '360deg']
        });
        const spinHide = this.state.spinAnim.interpolate({
            inputRange: [0, 1],
            outputRange: ['0deg', '0deg']
        });
        const {color} = this.props;
        console.log('its color', color);
        return (
            <KeyboardAvoidingView  style={{flex:1, backgroundColor: color}}>
                <StatusBar backgroundColor = {this.props.color}/>
                <SafeAreaView style={this.styles.settingIcon}>
                    <TouchableOpacity onPress={() => this.props.navigation.push('Options')}>
                        <Entypo name="cog" size={32} color={colors.white} />
                    </TouchableOpacity>
                </SafeAreaView>
                <ScrollView scrollEnabled = {this.state.IsScrollEnabled} style={{ paddingTop:screen.height * 0.1}}>
                    <View style={this.styles.logoContainer}>
                        <Animated.Image
                            source={require('../assets/images/background.png')}
                            style={{width: screen.width / 0.45, height: screen.width * 0.45,
                                transform: this.state.IsLogoRotationEnabled ? [{rotate: spinShow}] : [{rotate: spinHide}]}}
                            resizeMode="contain"
                        />
                        <Image
                            source={require('../assets/images/logo.png')}
                            style={this.styles.logo}
                            resizeMode="contain"
                        />
                    </View>
                    {this.props.processing ? <ActivityIndicator size="large" color={colors.white}/> :
                        <View>
                            <Text style={this.styles.appName}>Currency Converter</Text>
                            <CustomInput
                                text={this.state.baseCurrency}
                                value={this.state.baseCurrencyUserValue}
                                onButtonPress={() => this.props.navigation.push('CurrencyList', {
                                    title: 'Base Currency',
                                    activeCurrency: this.state.baseCurrency,
                                    onChange: (currency) => this.setBaseCurrency(currency, this.props.currencyRateList)
                                })}
                                keyboardType='number-pad'
                                onChangeText={text => this.setState({baseCurrencyUserValue: text.replace(/[^0-9]/g, '')})}
                                editable={true}
                                onSubmitEditing={Keyboard.dismiss}
                            />
                            <CustomInput
                                text={this.state.quoteCurrency}
                                value={`${(parseFloat(this.state.baseCurrencyUserValue) *
                                    this.state.conversionRate).toFixed(2)}`}
                                onButtonPress={() => this.props.navigation.push('CurrencyList', {
                                    title: 'Quote Currency',
                                    activeCurrency: this.state.quoteCurrency,
                                    onChange: (currency) => {
                                        this.setQuoteCurrency(currency, this.props.currencyRateList)
                                    }
                                })}
                                keyboardType="numeric"
                                editable={false}
                            />
                            <Text style={this.styles.text}>
                                {`1 ${this.state.baseCurrency} = ${this.state.conversionRate} ${this.state.quoteCurrency} as of ${currentDate}`}
                            </Text>
                            <ReverseButton text="Reverse Currencies"
                                           onPress={() => this.swapCurrencies(this.props.currencyRateList)}/>
                        </View>
                    }
                    <View style={{ height: this.state.IsScrollEnabled ? screen.height : 0 }} />
                </ScrollView>
            </KeyboardAvoidingView>
        )
    }
}

const mapStateToProps = (state) => {
    console.log('its home view  data', state);
    return {
        currencyRateList: state.currencyConversion.currencyRateList,
        baseCurrency: state.currencyConversion.baseCurrency,
        quoteCurrency: state.currencyConversion.quoteCurrency,
        baseCurrencyUserValue: state.currencyConversion.baseCurrencyUserValue,
        conversionRate: state.currencyConversion.conversionRate,
        processing: state.currencyConversion.processing,
        color: state.currencyConversion.color,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        loadCurrencyRates: (currency) => dispatch(loadCurrencyRates(currency)),
    }
};

export const _Home = connect(mapStateToProps, mapDispatchToProps)(Home);