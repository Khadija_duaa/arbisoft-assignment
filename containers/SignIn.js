import React from 'react';
import {
    Dimensions,
    SafeAreaView,
    StatusBar,
    StyleSheet,
    TextInput,
    View,
    TouchableOpacity,
    Text,
    ActivityIndicator
} from 'react-native';
import {connect} from "react-redux";
import {CustomButton} from "../components/CustomButton";
import { setUserAuthenticated} from "../stores/currency-conversion/currency-conversion-actions";
import colors from '../utils/colors';

const screen = Dimensions.get('window');

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#4F6D7A',
        flex:1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    Input: {
        marginVertical: screen.height * 0.010,
        paddingVertical: 7,
        width: screen.width * 0.85,
        borderRadius: 6,
        backgroundColor: colors.white,
    },
    Header: {
        fontWeight: 'bold',
        fontSize: 40,
        color: colors.white,
        textAlign: 'center',
        paddingVertical: 10,
    },
});

class SignIn extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            processing: false,
            testEmailInput: false,
            testPasswordInput: false,
            authenticated: false,
        };

    }

    utilFunc = (props) =>{
        this.props.setUserAuthenticated(true);
        setTimeout(() => {
            this.props.authenticated ? props.navigation.replace('Home') : props.navigation.push('SignIn')
        }, 300)
    };


    onButtonPress = (props) =>{
        this.setState({processing: true}, () =>{
         this.utilFunc(props)
        });

    };


    render() {
        return (

            <SafeAreaView style={styles.container}>
                {this.state.processing ? <ActivityIndicator size="large" color={colors.white}/> :
                    <View>
                        <StatusBar barStyle="dark-content" backgroundColor={colors.white}/>
                        <Text style={styles.Header}>{'WELCOME'}</Text>
                        <TextInput style={styles.Input} placeholder=" Type Email" onChangeText={(text) => {
                            this.setState({testEmailInput: true})
                        }}/>
                        <TextInput style={styles.Input} placeholder=" Type Password" secureTextEntry={true}
                                   onChangeText={(text) => {
                                       this.setState({testPasswordInput: true})
                                   }}/>
                        <CustomButton text={'Login'} disabled={
                            !(this.state.testEmailInput && this.state.testPasswordInput)} onPress={() => {
                            this.onButtonPress(this.props)
                        }}/>
                    </View>
                }
            </SafeAreaView>
        );
    }
}

const mapStateToProps = (state) => {

    return {
        authenticated: state.currencyConversion.authenticated,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        setUserAuthenticated: (boolean) => dispatch(setUserAuthenticated(boolean)),
    }
};


export const  _SignIn =connect(mapStateToProps, mapDispatchToProps)(SignIn);
