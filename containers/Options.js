import React from 'react';
import {SafeAreaView, StyleSheet, ScrollView, StatusBar, Linking, Alert} from 'react-native';
import { Entypo, SimpleLineIcons } from '@expo/vector-icons';
import {ListRow} from '../components/ListRow';
import {Separator} from '../components/Separator';
import colors from '../utils/colors';


const _styles = StyleSheet.create({
    separator: {
        backgroundColor: colors.border,
        height: 1,
        marginLeft: 20,
    },
});

const openExternalLink = url =>
    Linking.openURL(url).catch(() =>
        Alert.alert('Something went wrong.', 'Please try again later.')
    );

export const Options = (props) => {
    return (
        <SafeAreaView >
            <StatusBar barStyle="dark-content" backgroundColor={colors.white} />
            <ScrollView >
                <ListRow
                    title="Themes"
                    onPress={() => props.navigation.push('Themes')}
                    rightIcon={
                        <Entypo name="chevron-right" size={20} color={colors.text} />
                    }
                />

                <Separator style={_styles.separator}/>

                <ListRow
                    title="Fixer.io"
                    onPress={() => openExternalLink('https://fixer.io/'
                    )}
                    rightIcon={
                        <Entypo name="500px" size={20} color={colors.text} />
                    }
                />

                <Separator style={_styles.separator}/>

                <ListRow
                    title="Logout"
                    onPress={() => props.navigation.push('SignIn')}
                    rightIcon={
                        <SimpleLineIcons name="logout" size={20} color={colors.text} />
                    }
                />
            </ScrollView>
        </SafeAreaView>
    );
};