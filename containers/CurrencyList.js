import React from 'react';
import {StatusBar, FlatList, View, StyleSheet, Dimensions} from 'react-native';
import { MaterialCommunityIcons, Entypo} from '@expo/vector-icons';
import {connect} from "react-redux";
import {ListRow} from '../components/ListRow';
import {Separator} from '../components/Separator';
import {setFavoriteCurrencyList} from "../stores/currency-conversion/currency-conversion-actions";
import colors from '../utils/colors';

const screen = Dimensions.get('window');

const _styles = StyleSheet.create({
    separator: {
        backgroundColor: colors.border,
        height: 1,
    },
    ListItemContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    }
});

class CurrencyList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedFavorite: [] ,
        };
    }


    onIconPress = (item, setFavoriteCurrencyList) =>{
        let _selectedFavorite = [...this.state.selectedFavorite];
        _selectedFavorite.push({title:item.title});
        this.setState({ selectedFavorite: _selectedFavorite }, () =>{
            setFavoriteCurrencyList(this.state.selectedFavorite)});
    };

    render() {
        const { setFavoriteCurrencyList} = this.props;
        return (
            <View
                style={{
                    flex: 1,
                    backgroundColor: colors.white,
                }}
            >
                <StatusBar barStyle="dark-content" backgroundColor={colors.white}/>
                <FlatList
                    data={this.props && this.props.currencyList}
                    renderItem={({item}) => {
                        const selected = this.props.route.params.activeCurrency === item.title;
                        return (
                            <View style={_styles.ListItemContainer}>
                                <ListRow
                                    title={item.title}
                                    onPress={() => {
                                        this.props.route.params.onChange(item.title);
                                        this.props.navigation.pop();
                                    }}
                                    rightIcon={
                                        selected && <MaterialCommunityIcons
                                            style={{ marginHorizontal:screen.width * 0.01}}
                                            name="checkbox-marked-circle" size={24}
                                            color={this.props.color}/>
                                    }
                                />

                                <Entypo key ={item.title}
                                        name={item.iconName }
                                        size={25} color='#e70000' style={{ marginHorizontal:screen.width * 0.06}}
                                        onPress={() =>{ this.onIconPress(item,  setFavoriteCurrencyList)}}/>
                            </View>
                        );
                    }}
                    keyExtractor={item => item.title}
                    ItemSeparatorComponent={() => <Separator style={_styles.separator}/>}

                />
            </View>
        );
    }
}

const mapStateToProps = (state) => {
    console.log('its state', state);

    return {
        color: state.currencyConversion.color,
        currencyList: state.currencyConversion.currencyList,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        setFavoriteCurrencyList: (currencyList) => dispatch(setFavoriteCurrencyList(currencyList)),
    }
};

export const _CurrencyList = connect(mapStateToProps, mapDispatchToProps)(CurrencyList);