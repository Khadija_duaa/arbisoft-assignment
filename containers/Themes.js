import React from 'react';
import {SafeAreaView, StyleSheet, ScrollView, StatusBar, Alert, View} from 'react-native';
import {MaterialCommunityIcons} from '@expo/vector-icons';
import {connect} from "react-redux";
import {ListRow} from '../components/ListRow';
import {Separator} from '../components/Separator';
import {setThemeColor} from "../stores/currency-conversion/currency-conversion-actions";
import colors from '../utils/colors';


const _styles = StyleSheet.create({
    separator: {
        backgroundColor: colors.border,
        height: 1,
        marginLeft: 20,
    },
});


class Themes extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    themeList ={

        data:[
            {
                title: 'Blue',
                colorCode: '#4f6d7a',
                icon: <MaterialCommunityIcons name="checkbox-multiple-blank-circle" size={24} color={'#4F6D7A'} />

            },
            {
                title: 'Green',
                colorCode: '#2E8B57',
                icon: <MaterialCommunityIcons name="checkbox-multiple-blank-circle" size={24} color={'green'} />
            },
            {
                title: 'Red',
                colorCode: '#FF0000',
                icon: <MaterialCommunityIcons name="checkbox-multiple-blank-circle" size={24} color={'red'} />
            },
            {
                title: 'Yellow',
                colorCode: 'yellow',
                icon: <MaterialCommunityIcons name="checkbox-multiple-blank-circle" size={24} color={'yellow'} />
            },
            {
                title: 'pink',
                colorCode: 'pink',
                icon: <MaterialCommunityIcons name="checkbox-multiple-blank-circle" size={24} color={'pink'} />
            },
            {
                title: 'black',
                colorCode: 'black',
                icon: <MaterialCommunityIcons name="checkbox-multiple-blank-circle" size={24} color={'black'} />
            },
            {
                title: 'brown',
                colorCode: 'brown',
                icon: <MaterialCommunityIcons name="checkbox-multiple-blank-circle" size={24} color={'brown'} />
            },
            {
                title: 'grey',
                colorCode: 'grey',
                icon: <MaterialCommunityIcons name="checkbox-multiple-blank-circle" size={24} color={'grey'} />
            },
            {
                title: 'Coral',
                colorCode: 'coral',
                icon: <MaterialCommunityIcons name="checkbox-multiple-blank-circle" size={24} color={'coral'} />
            },
            {
                title: 'Olive',
                colorCode: 'olive',
                icon: <MaterialCommunityIcons name="checkbox-multiple-blank-circle" size={24} color={'olive'} />
            },
            {
                title: 'Violet',
                colorCode: 'palevioletred',
                icon: <MaterialCommunityIcons name="checkbox-multiple-blank-circle" size={24} color={'palevioletred'} />
            }
        ]
    };
    render() {
        return (
            <SafeAreaView>
                <StatusBar barStyle="dark-content" backgroundColor={colors.white}/>
                <ScrollView>
                    {this.themeList.data.map((item, index) =>{
                        return(
                            <View key ={index}>
                                <ListRow
                                    title={item.title}
                                    onPress={() => this.props.setThemeColor(item.colorCode)}
                                    rightIcon={item.icon}
                                />

                                <Separator style={_styles.separator}/>
                            </View>
                        )
                    })}
                </ScrollView>
            </SafeAreaView>
        );
    };
}

const mapStateToProps = (state) => {

    return {
        color: state.currencyConversion.color,
    }
};
const mapDispatchToProps = (dispatch) => {

    return {
        setThemeColor: (color) => dispatch(setThemeColor(color)),
    }
};

export const _Themes = connect(mapStateToProps, mapDispatchToProps)(Themes);