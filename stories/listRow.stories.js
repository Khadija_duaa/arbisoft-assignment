import React from 'react';
import { storiesOf } from '@storybook/react-native';
import { action } from '@storybook/addon-actions';
import {ListRow} from '../components/ListRow';
import { BufferView } from './decorators';

storiesOf('ListRow', module)
    .addDecorator(BufferView)
    .add('default', () => (
        <ListRow onPress={action('tapped-default')}/>
    ))
    .add('bold', () => (
        <ListRow onPress={action('tapped-bold')} bold ={true}>
            Press Me
        </ListRow>
    ));