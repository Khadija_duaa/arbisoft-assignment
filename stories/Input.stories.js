import React from 'react';
import { storiesOf } from '@storybook/react-native';
import { action } from '@storybook/addon-actions';
import {CustomInput} from '../components/CustomInput';
import { BufferView } from './decorators';

storiesOf('Input', module)
    .addDecorator(BufferView)
    .add('default', () => (
        <CustomInput onButtonPress={action('tapped-default')}/>
    ));